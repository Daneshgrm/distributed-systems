from pyspark import SparkContext
import time
import sys


def create_adjacency_mat(line):
    data_tmp = line.split(' ')
    return (int(data_tmp[0]), (int(data_tmp[1]), float(data_tmp[2])))

def compute_norm1(dict1, dict2):
    sum = 0
    for k, v in dict1.items():
        sum += abs(v-dict2[k])
    return sum

if __name__ == '__main__':
    # pre-declarations
    A_mat = {}
    pi_vec = {}
    norm1_val = 0
    counter = 0

    # getting input
    input_arg = sys.argv
    file = str(input_arg[1])
    teleport_probability = float(input_arg[2])
    max_iterations = int(input_arg[3])
    convergence_threshold = float(input_arg[4])


    #t0 = time.time()
    # creating spark context
    sc = SparkContext(master='local[4]')
    content = sc.textFile(file, minPartitions=4).map(lambda line: create_adjacency_mat(line))
    sc.setCheckpointDir('./checkpoints/')
    node_no = content.flatMap(lambda x: (x[0], x[1][0])).distinct().count()
    tmp = sc.parallelize([(k, 0) for k in range(1, node_no + 1)], numSlices=4)
    result = sc.parallelize([(k, 0) for k in range(1, node_no + 1)], numSlices=4)
    result.persist()

    A_mat_tmp = content.keys().distinct().collect()
    for i in range(1, node_no+1):
        A_mat[i] = (1-teleport_probability)/(node_no-1)
        pi_vec[i] = 1 / node_no   # pi_arr zero state value
    for index in A_mat_tmp:
        A_mat[index] = 0

    pi_vec_new = pi_vec
    pi_vec_old = pi_vec

    # broadcast some variables
    A_mat = sc.broadcast(A_mat)
    teleport_probability = sc.broadcast(teleport_probability)
    pi_vec = sc.broadcast(pi_vec)
    node_no = sc.broadcast(node_no)

    # compute total weight per row in for H_matrix
    total_weight_per_row = content.mapValues(lambda weight: weight[1]).reduceByKey(lambda a, b: a+b)
    total_weight_per_row.persist()
    # create H_matrix
    H_mat = content.join(total_weight_per_row).mapValues(lambda x: (x[0][0], ((1-teleport_probability.value)*x[0][1]/x[1])))
    H_mat.persist()
    total_weight_per_row.unpersist()
    while (compute_norm1(pi_vec_new, pi_vec_old) > convergence_threshold or compute_norm1(pi_vec_new, pi_vec_old) == 0) and counter < max_iterations:
        # compute first part of answer: (1-alpha)pi_vec*H_mat
        ans1 = H_mat.map(lambda x: (x[1][0], pi_vec.value[x[0]] * x[1][1])).reduceByKey(lambda a, b: a + b)
        ans1.persist()

        # compute second part of answer: (1-alpha)pi_vec*A_mat + (alpha/node_no)pi_vec*one_mat
        ans2 = tmp.map(lambda x: (1, A_mat.value[x[0]] * pi_vec.value[x[0]])).reduceByKey(lambda a, b: a + b)
        ans2 = ans2.flatMap(lambda x: [(k, x[1]) for k in range(1, node_no.value+1)])
        ans2 = ans2.map(lambda x: (x[0], x[1] - pi_vec.value[x[0]] * A_mat.value[x[0]] + teleport_probability.value * 1 / node_no.value))
        ans2.persist()

        # sum of first part and second part
        result = ans1.join(ans2).mapValues(lambda x: x[0]+x[1])
        result.checkpoint()

        # save pi_vec new value and old value
        for k, v in result.collect():
            pi_vec_new[k] = v
        pi_vec_old = pi_vec.value

        counter += 1
        # print(pi_vec_new, counter)
        pi_vec.destroy()
        pi_vec = sc.broadcast(pi_vec_new)
        ans1.unpersist()
        ans2.unpersist()

    result.coalesce(1).saveAsTextFile('./result')
    sc.stop()
    #print(time.time()-t0)